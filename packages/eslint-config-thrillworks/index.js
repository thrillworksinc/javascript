module.exports = {
    parser: 'babel-eslint',
    extends: [
        'airbnb',
        'prettier'
    ],
    env: {
        jest: true,
        node: true,
        browser: true
    },
    plugins: [
        'graphql',
        'prettier',
        'react'
    ],
    rules: {
        'no-console': [
            'error',
            {
              'allow': [
                'warn',
                'error'
              ]
            }
          ],
          'arrow-body-style': 'off',
          'arrow-parens': 'off',
          'indent': 'off',
          'object-curly-newline': 'off',
          'no-extra-parens': 'off',
          'no-use-before-define': 'off',
          'no-underscore-dangle': 'off',
          'react/prefer-stateless-function': 'off',
          'react/prop-types': 'error',
          'no-param-reassign': [
            'error',
            {
              'props': false
            }
          ],
          'new-cap': 'error',
          'import/named': 'error',
          'import/no-extraneous-dependencies': [
            'error',
            {
              'devDependencies': true
            }
          ],
          'import/extensions': [
            'off',
            'never'
          ],
          'import/no-useless-path-segments': 'error',
          'react/jsx-filename-extension': [
            'off',
            {
              'extensions': [
                '.js',
                '.jsx'
              ]
            }
          ],
          'react/jsx-indent': 'off',
          'no-mixed-operators': 'off',
          'function-paren-newline': 'off',
          'complexity': [
            'off',
            10
          ],
          'jsx-a11y/anchor-is-valid': [
            'error',
            {
              'components': [
                'Link'
              ],
              'specialLink': [
                'to'
              ]
            }
          ],
          'jsx-a11y/label-has-for': [
            'error',
            {
              'components': [
                'label'
              ],
              'required': {
                'every': [
                  'nesting',
                  'id'
                ]
              },
              'allowChildren': false
            }
          ],
          'no-else-return': [
            'error',
            {
              'allowElseIf': true
            }
          ],
          'react/no-access-state-in-setstate': 'error',
          'react/sort-comp': 'error',
          'react/destructuring-assignment': 'error',
          'require-await': 'error',
          'import/order': [
            'error',
            {
              'newlines-between': 'always'
            }
          ],
          'implicit-arrow-linebreak': 'off',
          'react/jsx-one-expression-per-line': 'off',
          'operator-linebreak': 'off'
    }
};