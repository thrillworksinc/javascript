module.exports = {
  parser: 'babel-eslint',
  extends: [
      'airbnb-base',
      'prettier'
  ],
  env: {
      jest: true,
      node: true,
      browser: true
  },
  plugins: [
      'prettier',
  ],
  rules: {
      'no-console': [
          'error',
          {
            'allow': [
              'warn',
              'error'
            ]
          }
        ],
        'arrow-body-style': 'off',
        'arrow-parens': 'off',
        'indent': 'off',
        'object-curly-newline': 'off',
        'no-extra-parens': 'off',
        'no-use-before-define': 'off',
        'no-underscore-dangle': 'off',
        'no-param-reassign': [
          'error',
          {
            'props': false
          }
        ],
        'new-cap': 'error',
        'import/named': 'error',
        'import/no-extraneous-dependencies': [
          'error',
          {
            'devDependencies': true
          }
        ],
        'import/extensions': [
          'off',
          'never'
        ],
        'import/no-useless-path-segments': 'error',
        'no-mixed-operators': 'off',
        'function-paren-newline': 'off',
        'complexity': [
          'off',
          10
        ],
        'no-else-return': [
          'error',
          {
            'allowElseIf': true
          }
        ],
        'require-await': 'error',
        'import/order': [
          'error',
          {
            'newlines-between': 'always'
          }
        ],
        'implicit-arrow-linebreak': 'off',
        'operator-linebreak': 'off'
  }
};